module gitlab.com/Feokrat/testing-go-gorilla

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20210313110737-8e9fff1a3a18 // indirect
)
