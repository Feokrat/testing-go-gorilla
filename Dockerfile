FROM golang:latest 

WORKDIR /app

RUN apt-get update && apt-get install -y curl

COPY . /app
 
CMD ["/app/mybinary.exe"]

EXPOSE 8080
