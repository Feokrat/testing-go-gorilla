package main

import (
	"flag"
	"log"

	"github.com/BurntSushi/toml"
	"gitlab.com/Feokrat/testing-go-gorilla/src/apiserver"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "src/configs/apiserver.toml", "Path to config.toml")
}

func main() {
	flag.Parse()

	config := apiserver.NewConfig()
	_, err := toml.DecodeFile(configPath, config)
	if err != nil {
		log.Fatal(err)
	}
	server := apiserver.NewAPIServer(config)

	err = server.OpenConnection()
	if err != nil {
		log.Fatal(err)
	}
}
