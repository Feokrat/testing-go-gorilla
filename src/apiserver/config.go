package apiserver

type Config struct {
	BindAddr string `toml:"BindAddr"`
	LogLevel string `toml:"LogLevel"`
}

func NewConfig() *Config {
	return &Config{
		BindAddr: ":8080",
		LogLevel: "debug",
	}
}
